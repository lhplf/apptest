using Microsoft.VisualStudio.TestTools.UnitTesting;
using UcPag;
namespace UcPagTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Verificar_Taxa_De_Juro_Gerada()
        {
            double esperado = 0.01;
            Juro juro = new Juro();
            double atual = juro.GerarTaxaJuro();
            Assert.AreEqual(esperado, atual, 0.001, "num foi");

        }
        [TestMethod]
        public void Verificar_Calculo_De_Juros()
        {
            double esperado = 105.10;
            double ini = 100;
            int tp = 5;
            Juro juro = new Juro();
            juro.CalculaJuros(ini,tp);
            Assert.AreEqual(esperado, juro.Result, 0.001, "num foi");

        }
        [TestMethod]
        public void Verifica_Ini_Igual_A_Zero()
        {
            double ini = 0;
            int tp = 5;
            Juro juro = new Juro();
            
            Assert.ThrowsException<System.ArgumentOutOfRangeException>(() => juro.CalculaJuros(ini, tp));
        }
        [TestMethod]
        public void Verifica_Parcela_Igual_A_Zero()
        {
            double ini = 100;
            int tp = 0;
            Juro juro = new Juro();

            Assert.ThrowsException<System.ArgumentOutOfRangeException>(() => juro.CalculaJuros(ini, tp));
        }
    }
}
